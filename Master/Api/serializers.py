from rest_framework.serializers import ModelSerializer
from RingoPetStore.models import producto

class ProductSerializer(ModelSerializer):
    class Meta:
        model = producto
        fields = '__all__'
        