from rest_framework.viewsets import ModelViewSet
from Api.serializers import ProductSerializer
from RingoPetStore.models import producto

class ProductApiViewSet(ModelViewSet):
    serializer_class = ProductSerializer
    queryset = producto.objects.all()
    