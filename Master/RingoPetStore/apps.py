from django.apps import AppConfig


class RingopetstoreConfig(AppConfig):
    name = 'RingoPetStore'
