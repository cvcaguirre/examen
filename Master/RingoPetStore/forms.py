from django import forms
from .models import usuario
from .models import producto
from .models import contacto



class usuarioForm(forms.ModelForm):
    class Meta:
        model = usuario
        fields = '__all__'

class productoForm(forms.ModelForm):
    class Meta:
        model = producto
        fields = '__all__'

class contactoForm(forms.ModelForm):
    class Meta:
        model = contacto
        fields = '__all__'
