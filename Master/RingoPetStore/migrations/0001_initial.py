# Generated by Django 4.0.4 on 2022-07-02 01:14

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='contacto',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombres', models.CharField(max_length=100, verbose_name='Nombres')),
                ('apellidoPaterno', models.CharField(max_length=100, verbose_name='Apellido Paterno')),
                ('apellidoMaterno', models.CharField(max_length=100, verbose_name='Apellido Materno')),
                ('email', models.EmailField(max_length=100, verbose_name='Email')),
                ('telefono', models.CharField(max_length=100, verbose_name='Telefono')),
                ('direccion', models.CharField(max_length=100, verbose_name='Direccion')),
                ('mensaje', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='producto',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('titulo', models.CharField(max_length=100, verbose_name='Titulo')),
                ('categoria', models.CharField(choices=[('Perro', 'Perro'), ('Gato', 'Gato'), ('Otro', 'Otro')], max_length=100, null=True, verbose_name='Categoria')),
                ('descripcion', models.TextField(null=True, verbose_name='Descripcion')),
                ('precio', models.IntegerField()),
                ('stock', models.IntegerField()),
                ('imagen', models.ImageField(null=True, upload_to='imagenes/', verbose_name='Imagen')),
            ],
        ),
        migrations.CreateModel(
            name='usuario',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('rut', models.CharField(max_length=10)),
                ('dv', models.CharField(max_length=1)),
                ('nombres', models.CharField(max_length=100, verbose_name='Nombres')),
                ('apellidoPaterno', models.CharField(max_length=100, verbose_name='Apellido Paterno')),
                ('apellidoMaterno', models.CharField(max_length=100, verbose_name='Apellido Materno')),
                ('nacionalidad', models.CharField(max_length=100, verbose_name='Nacionalidad')),
                ('email', models.EmailField(max_length=100, verbose_name='Email')),
            ],
        ),
    ]
