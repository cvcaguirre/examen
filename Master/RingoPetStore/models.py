from tokenize import Name
from django.db import models

# Create your models here.

class usuario(models.Model):
    id = models.AutoField(primary_key=True)
    rut = models.CharField(max_length=10)
    dv = models.CharField(max_length=1)
    nombres = models.CharField(max_length=100 , verbose_name="Nombres")
    apellidoPaterno = models.CharField(max_length=100 , verbose_name="Apellido Paterno")
    apellidoMaterno = models.CharField(max_length=100 , verbose_name="Apellido Materno")
    nacionalidad = models.CharField(max_length=100 , verbose_name="Nacionalidad")
    email = models.EmailField(max_length=100 , verbose_name="Email")

class producto(models.Model):
    id = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=100 , verbose_name="Titulo")
    categoria = models.CharField(max_length=100 , verbose_name="Categoria", choices=(('Perro','Perro'),('Gato','Gato'),('Otro','Otro')),null=True)
    descripcion = models.TextField(null=True , verbose_name="Descripcion")
    precio = models.IntegerField()
    stock = models.IntegerField()
    imagen = models.ImageField(upload_to='imagenes/',null=True , verbose_name="Imagen")

class contacto(models.Model):
    id = models.AutoField(primary_key=True)
    nombres = models.CharField(max_length=100 , verbose_name="Nombres")
    apellidoPaterno = models.CharField(max_length=100 , verbose_name="Apellido Paterno")
    apellidoMaterno = models.CharField(max_length=100 , verbose_name="Apellido Materno")
    email = models.EmailField(max_length=100 , verbose_name="Email")
    telefono = models.CharField(max_length=100 , verbose_name="Telefono")
    direccion = models.CharField(max_length=100 , verbose_name="Direccion")
    mensaje = models.TextField()

