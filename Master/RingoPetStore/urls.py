from django.urls import path
from . import views
from django.conf import settings
from django.contrib.staticfiles.urls import static
from django.contrib import admin
from django.urls import path, include





urlpatterns = [
	path('inicio', views.inicio, name='inicio'),
	path('success', views.logout, name='logout'),
	path('galeria', views.galeria,name='galeria'),
	path('PetStore', views.PetStore,name='PetStore'),
	path('PetStore/crear', views.crear_usuario,name='crear_usuario'),
	path('PetStore/editar', views.editar_usuario,name='editar_usuario'),
	path('PetStore/eliminar/<int:id>', views.eliminar_usuario,name='eliminar_usuario'),
	path('PetStore/editar/<int:id>', views.editar_usuario,name='editar_usuario'),
	path('PetStore/Productos', views.Productos,name='Productos'),
	path('PetStore/Productos/crear_producto', views.crear_producto,name='crear_producto'),
	path('PetStore/Productos/eliminar_producto/<int:id>', views.eliminar_producto,name='eliminar_producto'),
	path('PetStore/Productos/editar_producto/<int:id>', views.editar_producto,name='editar_producto'),
	path('PetStore/Contacto', views.Contacto,name='Contacto'),
	path('PetStore/Contacto/crear_contacto', views.crear_contacto,name='crear_contacto'),
	path('PetStore/Contacto/eliminar_contacto/<int:id>', views.eliminar_contacto,name='eliminar_contacto'),
	path('PetStore/Contacto/editar_contacto/<int:id>', views.editar_contacto,name='editar_contacto'),
	path('', views.editar_contacto,name='editar_contacto'),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)