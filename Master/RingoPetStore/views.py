from django.shortcuts import render , redirect
from django.http import HttpResponse
from .models import usuario
from .models import producto
from .models import contacto
from .forms import usuarioForm
from .forms import productoForm
from .forms import contactoForm
from django.contrib.auth.models import User, auth
from django.views.decorators.csrf import csrf_protect


def inicio(request):
	return render(request, 'views/inicio.html')

def galeria(request):
	return render(request, 'views/galeria.html')


#def de usuarios
def PetStore(request):
	usuarios = usuario.objects.all() # Obtenemos todos los usuarios
	return render(request, 'PetStore/index.html', {'usuario': usuarios})


def crear_usuario(request):
	formulario = usuarioForm(request.POST or None)
	if formulario.is_valid() and request.POST:
		formulario.save()
		return redirect('PetStore')
	return render(request, 'PetStore/crear.html', {'formulario': formulario})


def editar_usuario(request, id):
	editar_usuario = usuario.objects.get(id=id)
	formulario = usuarioForm(request.POST or None, instance=editar_usuario)
	if formulario.is_valid() and request.POST:
		formulario.save()
		return redirect('PetStore')
	return render(request, 'PetStore/editar.html', {'formulario': formulario})

def	eliminar_usuario(request, id):
	usuario_eliminar = usuario.objects.get(id=id)
	usuario_eliminar.delete()
	return redirect('PetStore')


#def de productos

def Productos(request):
	productos = producto.objects.all() # Obtenemos todos los productos
	return render(request, 'PetStore/Productos/index_productos.html', {'producto': productos})

def	crear_producto(request):
	formulario_productos = productoForm(request.POST or None, request.FILES or None)
	if formulario_productos.is_valid():
		formulario_productos.save()
		return redirect('Productos')
	return render(request, 'PetStore/Productos/crear_producto.html', {'formulario_producto': formulario_productos})

def editar_producto(request, id):
	producto_editar = producto.objects.get(id=id)
	formulario_productos = productoForm(request.POST or None, request.FILES or None, instance=producto_editar)
	if formulario_productos.is_valid() and request.POST:
		formulario_productos.save()
		return redirect('Productos')
	return render(request, 'PetStore/Productos/editar_producto.html', {'formulario_producto': formulario_productos})

def	eliminar_producto(request, id):
	producto_eliminar = producto.objects.get(id=id)
	producto_eliminar.delete()
	return redirect('Productos')


#def de contacto

def Contacto(request):
	contactos = contacto.objects.all() # Obtenemos todos los contactos realizados
	return render(request, 'PetStore/Contacto/index_contacto.html', {'contacto': contactos})

def	crear_contacto(request):
	formulario_contacto = contactoForm(request.POST or None)
	if formulario_contacto.is_valid():
		formulario_contacto.save()
		return redirect('inicio')
	return render(request, 'PetStore/Contacto/crear_contacto.html', {'formulario_contacto': formulario_contacto}) 

def editar_contacto(request, id):
	contacto_editar = contacto.objects.get(id=id)
	formulario_contacto = contactoForm(request.POST or None, instance=contacto_editar)
	if formulario_contacto.is_valid() and request.POST:
		formulario_contacto.save()
		return redirect('inicio')
	return render(request, 'PetStore/Contacto/editar_contacto.html', {'formulario_contacto': formulario_contacto})

def	eliminar_contacto(request, id):
	contacto_eliminar = contacto.objects.get(id=id)
	contacto_eliminar.delete()
	return redirect('inicio')

def logout(request):
    auth.logout(request)
    return redirect('/')